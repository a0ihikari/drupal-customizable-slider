<?php
/**
 * @file
 * Template file for the theming of the slider. Since the slider uses it's own markup, we want to display it in the same form.
 *
 * @modified by Weverson G. Santos, BYUCE Web Team <aoi_hikari@hotmail.com>
 *
 * Available custom variables:
 * - $nodes: An array of node objects
 * - $randomize: Boolean if randomized feature is turned on(TRUE) or off(False).
 * - $num: The ID number for the YouTube iframes
 *
 */
$validate = false;
$num = 0;
$num1 = 0;
$num2 = 0;
$num3 = 0;
$html = '<script>'.
			'var tag = document.createElement("script");'.
			'tag.src = "https://www.youtube.com/iframe_api";'.
			'var firstScriptTag = document.getElementsByTagName("script")[0];'.
			'firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);';
			foreach ($nodes as $node){
				$youtube = field_get_items('node', $node, 'byuce_feature_slider_youtube'); 
				$youtube = field_view_value('node', $node, 'byuce_feature_slider_youtube', $youtube[0]);
				if ($youtube['#markup'] == "1"){
					$num++;
					$html .='var player'.$num.';';
				}
			}
			$num = 0;
$html .='</script>';
foreach ($nodes as $node){
	$path = field_get_items('node', $node, 'byuce_feature_slider_path');
	$path = field_view_value('node', $node, 'byuce_feature_slider_path', $path[0]);
	if ($path['#markup'] == current_path()  || $path['#markup'] == "all"){
		$validate = true;
	}
}
if ($validate == true){	
	$html .= 
		'<div id="feature">'.
			'<ul id="slider">';
}		
		//Implement the YouTube iframe API
		
		foreach ($nodes as $node){			
			//Pull the info out from the node object then set some variable names so it's easier to work with. 
			$title = ($node->{'title'});
			
			$body = field_get_items('node', $node, 'body'); 
			$body = field_view_value('node', $node, 'body', $body[0]); 
			
			$color = field_get_items('node', $node, 'byuce_feature_slider_color');
			$color = field_view_value('node', $node, 'byuce_feature_slider_color', $color[0]); 
			
			$border_color = field_get_items('node', $node, 'byuce_feature_slider_border');
			$border_color = field_view_value('node', $node, 'byuce_feature_slider_border', $border_color[0]); 
			
			$title_color = field_get_items('node', $node, 'byuce_feature_slider_title_color'); 
			$title_color = field_view_value('node', $node, 'byuce_feature_slider_title_color', $title_color[0]);
			
			$text_color = field_get_items('node', $node, 'byuce_feature_slider_text_color'); 
			$text_color = field_view_value('node', $node, 'byuce_feature_slider_text_color', $text_color[0]);
						
			$color_hb = field_get_items('node', $node, 'byuce_feature_slider_color_hb');
			$color_hb = field_view_value('node', $node, 'byuce_feature_slider_color_hb', $color_hb[0]); 
			
			$border_color_hb = field_get_items('node', $node, 'byuce_feature_slider_border_hb');
			$border_color_hb = field_view_value('node', $node, 'byuce_feature_slider_border_hb', $border_color_hb[0]); 
			
			$text_color_hb = field_get_items('node', $node, 'byuce_feature_slider_text_hbc'); 
			$text_color_hb = field_view_value('node', $node, 'byuce_feature_slider_text_hbc', $text_color_hb[0]);
						
			$bottom_top = field_get_items('node', $node, 'byuce_feature_slider_bt'); 
			$bottom_top = field_view_value('node', $node, 'byuce_feature_slider_bt', $bottom_top[0]);
			$bottom_top = render($bottom_top);
			
			$right_left = field_get_items('node', $node, 'byuce_feature_slider_rl'); 
			$right_left = field_view_value('node', $node, 'byuce_feature_slider_rl', $right_left[0]);
			$right_left = render($right_left);
			
			$image = field_get_items('node', $node, 'byuce_feature_slider_image');
			$alt = $image[0]['alt']; 
			$image = file_create_url($image[0]['uri']);
			
			$video = field_get_items('node', $node, 'byuce_feature_slider_video');
			$video = field_view_value('node', $node, 'byuce_feature_slider_video', $video[0]);
			
			$path = field_get_items('node', $node, 'byuce_feature_slider_path');
			$path = field_view_value('node', $node, 'byuce_feature_slider_path', $path[0]);
			
			$h_bar = field_get_items('node', $node, 'byuce_feature_slider_hbar');
			$h_bar = field_view_value('node', $node, 'byuce_feature_slider_hbar', $h_bar[0]);
			
			$youtube = field_get_items('node', $node, 'byuce_feature_slider_youtube'); 
			$youtube = field_view_value('node', $node, 'byuce_feature_slider_youtube', $youtube[0]);
			
			$vbg_color = field_get_items('node', $node, 'byuce_feature_slider_vbg_color'); 
			$vbg_color = field_view_value('node', $node, 'byuce_feature_slider_vbg_color', $vbg_color[0]);
			
			$vbgc_bw = field_get_items('node', $node, 'byuce_feature_slider_vbg_bw'); 
			$vbgc_bw = field_view_value('node', $node, 'byuce_feature_slider_vbg_bw', $vbgc_bw[0]);
			
			$link = field_get_items('node', $node, 'byuce_feature_slider_link'); 
			$link = field_view_value('node', $node, 'byuce_feature_slider_link', $link[0]);
			$link = $link["#element"]["url"];
			
			$logo = field_get_items('node', $node, 'byuce_feature_slider_logo');
			$alt_logo = $logo[0]['alt']; 
			$logo = file_create_url($logo[0]['uri']);
			
			$hv = field_get_items('node', $node, 'byuce_feature_slider_hv'); 
			$hv = field_view_value('node', $node, 'byuce_feature_slider_hv', $hv[0]);
			
			$position = field_get_items('node', $node, 'byuce_feature_slider_position'); 
			$position = field_view_value('node', $node, 'byuce_feature_slider_position', $position[0]);
			
			//End variable declarations

			//Stores the video URL if there is one

				$video_url = $video['#markup'];

			global $base_url;
			if($image == ''.$base_url.'/'){				
				$image = "";
			}
			if($logo == ''.$base_url.'/'){
				$logo = "";
			}
			//Compares $path with the URL path
		
		if ($path['#markup'] == current_path()  || $path['#markup'] == "all"){
			$num3++;
			if ($bottom_top == "Top"){
				$num2++;
				$html .='<li class="feature">'.
							'<div></div>'.
							'<div class="size'.$num3.'">'.
							'<div class="feature-descriptions'.$num2.'">'.
								'<p>'.render($h_bar).'</p>'.
							'</div>';
				$num2--;			
			} else {
				$html .= '<li class="feature">'.
							'<div></div>'.
							'<div class="size'.$num3.'">';
			}
		
			if ($right_left == "Right" || $right_left == "Left"){			
				if ($right_left == "Left"){
					if (empty($video_url)){
						
						$html .='<div ';$num1++;if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><div class="feature-description'.$num1.'">'.
								'<h2><a href="'.$link.'">'.$title.'</a></h2>'.
								'<p>'.render($body).'</p>'.
							'</div><div class="feature-images' ;if ($bottom_top == "Top"){$html .=' top';}$html .='">';
							if (!empty($logo)){
							$html .='<img class=';if ($hv['#markup'] == "Horizontal"){
									if ($position['#markup'] == "Top Left"){
										$html .='"logo-h"';
									} elseif ($position['#markup'] == "Top Right"){
										$html .='"logo-hr"';
									} elseif ($position['#markup'] == "Bottom Left"){
										if ($bottom_top == "Bottom"){
											$html .='"logo-hbb"';
										} else {
											$html .='"logo-hb"';
										}
									}else {
									if ($bottom_top == "Bottom"){
											$html .='"logo-hbrb"';
										} else {
											$html .='"logo-hbr"';
										}
									}
								} else {
									if ($position['#markup'] == "Top Left"){
										$html .='"logo-v"';
									} elseif ($position['#markup'] == "Top Right"){
										$html .='"logo-vrl"';
									}elseif ($position['#markup'] == "Bottom Left"){
										if ($bottom_top == "Bottom"){
											$html .='"logo-vblb"';
										} else {
											$html .='"logo-vbl"';
										}
									}else {
										if ($bottom_top == "Bottom"){
											$html .='"logo-vbrb"';
										} else {
											$html .='"logo-vbr"';
										}	
									}
								}
							$html .='src="'.$logo.'" alt="'.$alt_logo.'">';
							}
							$html .='<img class="l" src="'.$image.'" alt="'.$alt.'"></div>'; $num1--;	
						
					} else {			
						if ($youtube['#markup'] == "1"){
							//Display a YouTube video.
							$num++;
							if ($bottom_top == "Top" || $bottom_top == "Bottom"){
								$html .= '<div ';$num1++;if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><div class="feature-description'.$num1.'">'.
								'<h2><a href="'.$link.'">'.$title.'</a></h2>'.
								'<p>'.render($body).'</p>'.
							'</div><div id="player" >'. 
											'<div id="player'.$num.'" class="feature-videos';if ($bottom_top == "Top"){$html .=' top';}$html .='">'.
										'</div></div>';$num1--;
							} else {
								$html .= '<div ';$num1++;if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><div class="feature-description'.$num1.'">'.
								'<h2><a href="'.$link.'">'.$title.'</a></h2>'.
								'<p>'.render($body).'</p>'.
							'</div><div id="player'.$num.'" class="feature-videos"></div>';$num1--;
							}
										
						} else {
							//video display using the video tag. 
							$html .= '<div ';$num1++;if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><div class="feature-description'.$num1.'">'.
								'<h2><a href="'.$link.'">'.$title.'</a></h2>'.
								'<p>'.render($body).'</p>'.
							'</div><div><video class="feature-videos';if ($bottom_top == "Top"){$html .=' top';}$html .='" width="699" height="324" controls poster="'.$image.'"><source src="'.$video_url.'" type="video/mp4"></video></div>';$num1--;
						}					
					}
				} else {
					if (empty($video_url)){
						$html .= '<div ';if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='>';
						if (!empty($logo)){
							$html .='<img class=';if ($hv['#markup'] == "Horizontal"){
								if ($position['#markup'] == "Top Left"){
									$html .='"logo-h"';
								} elseif ($position['#markup'] == "Top Right"){
									$html .='"logo-hrr"';
								}elseif ($position['#markup'] == "Bottom Left"){
									if ($bottom_top == "Bottom"){
										$html .='"logo-hblb"';
									} else {
										$html .='"logo-hbl"';
									}
								}else {
									if ($bottom_top == "Bottom"){
										$html .='"logo-hbrrb"';
									} else {
										$html .='"logo-hbrr"';
									}	
								}
							} else {
								if ($position['#markup'] == "Top Left"){
									$html .='"logo-v"';
								} elseif ($position['#markup'] == "Top Right"){
									$html .='"logo-vrr"';
								}elseif ($position['#markup'] == "Bottom Left"){
									if ($bottom_top == "Bottom"){
										$html .='"logo-vbb"';
									} else {
										$html .='"logo-vb"';
									}
								}else {
									if ($bottom_top == "Bottom"){
										$html .='"logo-vbrrb"';
									} else {
										$html .='"logo-vbrr"';
									}	
								}
							}
							$html .='src="'.$logo.'" alt="'.$alt_logo.'">';
							}
						$html .='<img class="features';if ($bottom_top == "Top"){$html .=' top';}$html .='" src="'.$image.'" alt="'.$alt.'">';		
					} else {			
						if ($youtube['#markup'] == "1"){
							//Display a YouTube video.
							$num++;
							if ($bottom_top == "Top" || $bottom_top == "Bottom"){
								$html .= '<div id="player" >'. 
											'<div id="player'.$num.'" class="features';if ($bottom_top == "Top"){$html .=' top';}$html .='">'.
										'</div>';
							} else {
								$html .= '<div id="player" >'.
											'<div id="player'.$num.'" class="features"></div>';
							}
										
						} else {
							//video display using the video tag. 
							$html .= '<div ';if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><video class="features';if ($bottom_top == "Top"){$html .=' top';}$html .='" width="699" height="324" controls poster="'.$image.'"><source src="'.$video_url.'" type="video/mp4"></video>';
						}					
					}
				}	
			} else {
			//Sets the slide to its non-description configuration 
				if (empty($video_url)){
					$html .= '<div ';if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='>';
					if (!empty($logo)){
							$html .='<img class=';if ($hv['#markup'] == "Horizontal"){
								if ($position['#markup'] == "Top Left"){
									$html .='"logo-h"';
								} elseif ($position['#markup'] == "Top Right"){
									$html .='"logo-hr"';
								}elseif ($position['#markup'] == "Bottom Left"){
									if ($bottom_top == "Bottom"){
										$html .='"logo-hbb"';
									} else {
										$html .='"logo-hb"';
									}
								}else {
									if ($bottom_top == "Bottom"){
										$html .='"logo-hbrb"';
									} else {
										$html .='"logo-hbr"';
									}
								}
							} else {
								if ($position['#markup'] == "Top Left"){
									$html .='"logo-v"';
								} elseif ($position['#markup'] == "Top Right"){
									$html .='"logo-vr"';
								}elseif ($position['#markup'] == "Bottom Left"){
									if ($bottom_top == "Bottom"){
										$html .='"logo-vblb"';
									} else {
										$html .='"logo-vbl"';
									}
								}else {
									if ($bottom_top == "Bottom"){
										$html .='"logo-vbrb"';
									} else {
										$html .='"logo-vbr"';
									}	
								}
							}
							$html .='src="'.$logo.'" alt="'.$alt_logo.'">';
					}
					$html .='<img class="feature-image';if ($bottom_top == "Top"){$html .=' top';}$html .='" src="'.$image.'" alt="'.$alt.'">';
				} else {			
					if ($youtube['#markup'] == "1"){
						//Display a YouTube video.
						$num++;
						if ($bottom_top == "Top" || $bottom_top == "Bottom"){
							$html .= '<div id="player" >'. 
										'<div id="player'.$num.'" class="feature-video';if ($bottom_top == "Top"){$html .=' top';}$html .='">'.
									'</div>';
						} else {
							$html .= '<div id="player" >'.
										'<div id="player'.$num.'" class="feature-video"></div>';
						}
					} else {
						//video display using the video tag. 
						$html .= '<div ';if ($bottom_top == "Top" || $bottom_top == "Bottom"){$html .='class="bt"';}$html .='><video class="feature-video';if ($bottom_top == "Top"){$html .=' top';}$html .='" width="940" height="324" controls poster="'.$image.'"><source src="'.$video_url.'" type="video/mp4"></video>';
					}
				}
			}					
				//Display descriptions

			if ($bottom_top == "Top" || $bottom_top == "Bottom"){
					$num2++;
					$html .='<style>'.
							'@media (min-width: 940px) {'.
								'.feature-descriptions'.$num2.' {';
								if ($bottom_top == "Top"){
									$html .= 'top: 0px;';
								} else {
									$html .= 'bottom: 0px;'.
										'float: left;';
								}	
								$html .='width: 940px;'.
								'background-color: '.$color_hb['#markup'].';'.
								'height: 30px;'.
								'border-left: 1px solid '.$border_color_hb['#markup'].';'.
								'box-shadow: inset 0 0 40px 5px '.$border_color_hb['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);'.
								'}'.
								'.feature-descriptions'.$num2.' p {'.
								'line-height: 29px;'.
								'margin: 0px 20px 0px;'.
								'text-align: center;';
						$html .='color: '.$text_color_hb['#markup'].';';
								$html .='}'.
								'}';
							$html .='@media (max-width: 698px) {'.
								'.feature-descriptions'.$num2.' {';
								$html .='width: 100%;'.
								'background-color: '.$color_hb['#markup'].';'.
								'padding: 2% 0;'.
								'border-left: 1px solid '.$border_color_hb['#markup'].';'.
								'box-shadow: inset 0 0 40px 5px '.$border_color_hb['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);'.
								'}'.
								'.feature-descriptions'.$num2.' p {'.
								'line-height: 17px;'.
								'margin: 0 2%;'.
								'text-align: center;';
						$html .='color: '.$text_color_hb['#markup'].';';
								$html .='}'.
								'}';
							$html .='@media (min-width : 699px) and (max-width : 939px) {'.
								'.feature-descriptions'.$num2.' {';
								if ($bottom_top == "Top"){
									$html .= 'top: 0px;';
								} 
								$html .='width: 100%;'.
								'background-color: '.$color_hb['#markup'].';'.
								'padding: 1% 0%;'.
								'border-left: 1px solid '.$border_color_hb['#markup'].';'.
								'box-shadow: inset 0 0 40px 5px '.$border_color_hb['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);'.
								'}'.
								'.feature-descriptions'.$num2.' p {'.
								'line-height: 17px;'.
								'margin: 0 0;'.
								'text-align: center;';
						$html .='color: '.$text_color_hb['#markup'].';';
						$html .='}'.
								'}'.
							'</style>';			
					if ($bottom_top == "Bottom"){
						$html .='<div class="feature-descriptions'.$num2.'">'.
									'<p>'.render($h_bar).'</p>'.
								'</div>';
					}
			}			
			if ($right_left == "Right" || $right_left == "Left"){	
					$num1++;
					$html .= '<style>';
						$html .='@media (min-width : 699px) and (max-width : 939px) {'.
								'.feature-description'.$num1.' {';																	
								$html .='padding: 2% 0%;'.
										'width: 100%;'.
										'background-color: '.$color['#markup'].';'.																		
										'border-left: 1px solid '.$border_color['#markup'].';'.
										'box-shadow: inset 0 0 110px 5px '.$border_color['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);';
								$html .='color: '.$text_color['#markup'].';';
								$html .='}'.
								'.feature-description'.$num1.' h2 {'.
								'font-size: 22px;'.
								'line-height: 30px;'.
								'margin: 0 2%;'.
								'}'.
								'.feature-description'.$num1.' p {'.
								'margin: 0 2%;'.
								'}';
						$html .='.feature-description'.$num1.' a, .feature-description'.$num1.' a:link, .feature-description'.$num1.' a:visited, .feature-description'.$num1.' a:active, .feature-description'.$num1.' a:hover, .feature-description'.$num1.' a:focus {'.
								'color: '.$title_color['#markup'].';'.
								'opacity: 1;'.
								'}';
						$html .='.feature-description'.$num1.' p {'.
								'font-size: 14px;'.
								'}';
							
							$html .='}';
							$html .='@media (max-width: 698px) {'.
								'.feature-description'.$num1.' {';	
								$html .='padding: 2% 0;'.
										'width: 100%;'.
										'background-color: '.$color['#markup'].';'.
										'border-left: 1px solid '.$border_color['#markup'].';'.
										'box-shadow: inset 0 0 110px 5px '.$border_color['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);';
								$html .='color: '.$text_color['#markup'].';';
								$html .='}'.
								'.feature-description'.$num1.' h2 {'.
								'font-size: 22px;'.
								'line-height: 30px;'.
								'margin: 0 2%;'.
								'}'.
								'.feature-description'.$num1.' p {'.
								'margin: 0 2%;'.
								'}';
						$html .='.feature-description'.$num1.' a, .feature-description'.$num1.' a:link, .feature-description'.$num1.' a:visited, .feature-description'.$num1.' a:active, .feature-description'.$num1.' a:hover, .feature-description'.$num1.' a:focus {'.
								'color: '.$title_color['#markup'].';'.
								'opacity: 1;'.
								'}';
						$html .='.feature-description'.$num1.' p {'.
								'font-size: 14px;'.
								'}';

							$html .='}';
							$html .='@media (min-width: 940px) {'.
								'.feature-description'.$num1.' {';
								if ($right_left == "Left"){
									if ($bottom_top == "Top"){
										$html .= 'bottom: 0;'.
												'z-index: -1;'.
												'position:relative;';
									} else {
										$html .= 'z-index: -1;'.
												'position:relative;';
									}
									$html .= 'float: left;';
								} else {
									if ($bottom_top == "Bottom"){
										$html .= 'top: 0px;'.
												'position: absolute;'.
												'right: 0;'.
												'z-index: -1;';
									} elseif ($bottom_top == "Top"){ 
										$html .= 'bottom: 0px;'.
											'position: absolute;'.
											'right: 0;'.
											'z-index: -1;';
									} else {
										$html .= 'float: right;';
									}
								}	
								$html .='padding: 0% 2.66%;'.
										'width: 190px;'.
										'background-color: '.$color['#markup'].';'.
										'height: 324px;'.								
										'border-left: 1px solid '.$border_color['#markup'].';'.
										'line-height: 17px;'.
										'box-shadow: inset 0 0 110px 5px '.$border_color['#markup'].', 0 0 12px rgba(0, 0, 0, 0.25);';
								$html .='color: '.$text_color['#markup'].';';
								$html .='}'.
								'.feature-description'.$num1.' h2 {'.
								'font-size: 22px;'.
								'line-height: 30px;'.
								'margin: 20px 0px 10px;'.
								'}';
						$html .='.feature-description'.$num1.' a, .feature-description'.$num1.' a:link, .feature-description'.$num1.' a:visited, .feature-description'.$num1.' a:active, .feature-description'.$num1.' a:hover, .feature-description'.$num1.' a:focus {'.
								'color: '.$title_color['#markup'].';'.
								'opacity: 1;'.
								'}';
						$html .='.feature-description'.$num1.' p {'.
								'font-size: 14px;'.
								'}';		
					$html .='}'.
					'</style>';
							if ($right_left == "Right"){
								$html .= '<div class="feature-description'.$num1.'">'.
									'<h2><a href="'.$link.'">'.$title.'</a></h2>'.
									'<p>'.render($body).'</p>'.
								'</div>';
							}
			}
			$html .= '<style>'.
					'div.size'.$num3.' {'.
						'overflow: hidden;';
						if ($vbgc_bw['#markup'] == "Black"){
							$html .='background-color: black;';
						} elseif ($vbgc_bw['#markup'] == "White") {
							$html .='background-color: white;';
						} else {
							if($vbg_color['#markup'] == "") {
								$html .='background-color: transparent;';
							} else {
								$html .='background-color: '.$vbg_color['#markup'].';';
							}
						}
				$html .='position: relative;'.
						'z-index: -20;'.
					'}'.
					'</style>';
			
			//end if radomize
		} 
		}
		if ($validate == true){	
			$html .='</div></li>';
			$html .= '</ul>'.
				'</div>';
		}			
		//Creates the YouTube iframe for each YouTube video
		$html .= '<script>'.
					'function onYouTubeIframeAPIReady() {';
					$num = 0;
					foreach ($nodes as $node){
										
						$youtube = field_get_items('node', $node, 'byuce_feature_slider_youtube'); 
						$youtube = field_view_value('node', $node, 'byuce_feature_slider_youtube', $youtube[0]);
						
						$video = field_get_items('node', $node, 'byuce_feature_slider_video');
						$video = field_view_value('node', $node, 'byuce_feature_slider_video', $video[0]);
										
						$right_left = field_get_items('node', $node, 'byuce_feature_slider_rl'); 
						$right_left = field_view_value('node', $node, 'byuce_feature_slider_rl', $right_left[0]);
						$right_left = render($right_left);
												
						if($video){
							$video_url = $video['#markup'];
						} else {
							$video_url = "";
						}
						if ($youtube['#markup'] == "1"){
							$num++;
							$html .='player'.$num.' = new YT.Player("player'.$num.'", {';
								if ($right_left == "Right" || $right_left == "Left"){
									$html .='height: "324",'.
									'width: "699",';
								} else {
									$html .='height: "324",'.
									'width: "940",';
								}
								$html .=
										'videoId: "'.$video_url.'",'.
										'events: {'.
											'"onReady": onPlayerReady,'.
										'}'.
									'});';										
						}
					}
					$html .='}'.
						'function onPlayerReady(event) {'.
							'event.target.stopVideo();';
					$html .='}'.
		'</script>';	
						
print $html;

?>
	