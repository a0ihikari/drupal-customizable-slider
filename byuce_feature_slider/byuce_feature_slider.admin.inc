<?php

/**
 * @file
 * Administration page callback from the BYUCE Feature Slider Module
 * This is so admins can set the settings 
 * 
 * @author Ephraim Sng <ephraim.sng@gmail.com>
 * 
 */

/**
 * Form to configure slider settings
 * 
 * @return type
 */
function byuce_feature_slider_admin_settings(){
	$form['settings'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Featured Slider Settings'),
	  '#description' => t('Configure the settings for the BYUCE featured slider'),
	);
	
	$form['settings']['byuce_slider_randomize'] = array(
	  '#type' => 'checkbox',
	  '#title' => t('Randomize Slides'),
	  '#description' => t('Have the featured images appear in random order whenever the slider is loaded'),
	  '#default_value' => variable_get('byuce_slider_randomize'),
	);
	
	return system_settings_form($form);
	
}
