BYUCE-Feature-Slider
============================

Drupal module to add a feature slider to the Continuing Education themes.

To use the spectrum color widget, download the bgrins-spectrum library from https://github.com/bgrins/spectrum and clone into sites/all/libraries folder.